package game;

public class Ship {
    private String name;
    private int displacement;
    private Player owner;
    ShipType shipType;
    /*static final String shipType1 = "Torpedo boat";
    static final String shipType2 = "Destroyer";
    static final String shipType3 = "Cruiser";
    static final String shipType4 = "Battleship";*/

    public Ship(ShipType shipType, int displacement, Player owner) {
        this.owner = owner;
        this.name = shipType.getName();
        this.displacement = displacement;
    }

    public Ship(String name, Player owner) {
        this.name = name;
    }

    public Ship(int displacement, Player owner) {
        this.displacement = displacement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public Player getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", displacement=" + displacement +
                ", owner=" + owner +
                ", shipType=" + shipType +
                '}';
    }
}
