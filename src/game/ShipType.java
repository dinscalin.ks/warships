package game;

public enum ShipType {
    TORPEDO_BOAT("Торпедный катер", "одно палубный", 1, 4),
    DESTROYER("Эсминец", "двух палубный", 2, 3),
    CRUISER("Крейсер", "трех палубный", 3, 2),
    BATTLESHIP("Линкор", "четфрех палубный", 4, 1);

    private String name;
    private String type;
    private int hp;
    private int count;


    ShipType(String name, String type, int hp, int count) {
        this.name = name;
        this.type = type;
        this.hp = hp;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getCount() {
        return count;
    }
}
