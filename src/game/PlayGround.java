package game;

public class PlayGround {
    private Player ownerPlayGround;
    private int[][] playGround = new int[10][10];
    private static final int[] numberOfShipTypes = {4, 3, 2, 1}; //см Ship shipType

    public PlayGround(Player ownerPlayGround) {
        this.ownerPlayGround = ownerPlayGround;
    }

    public void cellStatusPresent(int x, int y) { //присутствует
        playGround[x][y] = 1;
    }

    public void cellStatusMiss(int x, int y) { //промах
        playGround[x][y] = 2;
    }

    public void cellStatusHit(int x, int y) { //попал
        playGround[x][y] = 3;
    }

    public void cellStatusSank(int x, int y) { //затонул
        playGround[x][y] = 4;
    }
    public int playGroundStatus() {  // вывод поля
        for(int i = 0; i < playGround.length; i++) {
            for(int j = 0; j < playGround[i].length; j++) {
                System.out.print(playGround[i][j] + "  ");
            } System.out.println("");
        }
        return 0;
    }
    public static int[] getNumberOfShipTypes() {
        return numberOfShipTypes;
    }
}
