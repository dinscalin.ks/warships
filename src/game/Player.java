package game;

public class Player {
    private int playerScore;
    private String name;
    private PlayGround playGround;
    private Ship[] ships;

    public Player(String name, int playerScore) {
        this.playerScore = playerScore;
        this.name = name;
        this.ships = new Ship[shipCount()];
    }

    public Player(int playerScore) {
        this.playerScore = playerScore;
    }

    public Player(String name) {
        this.name = name;
    }

    public String shot(int x, int y) {
        return "Игрок: " + getName() + " решил ударить по ячейке - " + x + " x " + y;
    }

    public void setPlayGround(PlayGround playGround) { //set field
        this.playGround = playGround;
    }

    public void playerScoreAdd() {
        this.playerScore = getPlayerScore() + 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlayerScore() {
        return playerScore;
    }

    public void setPlayerScore(int playerScore) {
        this.playerScore = playerScore;
    }

    public static int shipCount() {
        int count = 0;
        for(int i = 0; i < PlayGround.getNumberOfShipTypes().length; i++) {
            count += PlayGround.getNumberOfShipTypes()[i];
        }
        return count;
    }

    //TODO сей метод в конструктор не пролазит
    /*public Ship[] generationShips(Player player) {  // TODO надо подумать куда его воткнуть!
        for(int i = 0; i < PlayGround.getNumberOfShipTypes().length; i++) {
            for(int j = 0; j < PlayGround.getNumberOfShipTypes()[i]; j++) {
                ships[j] = new Ship(Ship.ShipType, i + 1, player);
            }
        } return ships;
    }*/
}
